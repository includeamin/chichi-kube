FROM ubuntu:18.04
RUN apt-get update -y && \ 
    apt-get install -y mongodb
# RUN echo "Backup in progress"
# RUN mongodump --host mongo --port 37037 
# RUN echo "backup done"
# RUN echo "start to restore"
# RUN mongorestore --host chichiapp.ir --port 32768
# RUN echo "Restore Done"

CMD ["mongodump","--host","mongo","--port"," 37037","&&","mongorestore","--host","chichiapp.ir","--port","32768"]